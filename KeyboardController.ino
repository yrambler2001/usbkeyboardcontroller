//STM32DUINO
//https://wiki.stm32duino.com/
#include <USBComposite.h>
/*
Write to Keyboard.cpp builtin library:
size_t HIDKeyboard::release_direct(uint8_t k)
{
    uint8_t modifiers=0;    
    if (k != 0) {
        for (unsigned i=0; i<HID_KEYBOARD_ROLLOVER; i++) {
             if (keyReport.keys[i] == k) {
                 keyReport.keys[i] = 0;
                 break;
             }
        }
    }
    else {
        if (modifiers == 0)
            return 0;
    }
    
    keyReport.modifiers &= ~modifiers;
    
  sendReport();
  return 1;
}
size_t HIDKeyboard::press_direct(uint8_t k) {
    uint8_t modifiers=0;
    
    if (k == 0) {
        if (modifiers == 0) {
            return 0;
        }
    }
    else {
        for (unsigned i = 0; i<HID_KEYBOARD_ROLLOVER; i++) {
            if (keyReport.keys[i] == k) {
                goto SEND;
            }
        }
        for (unsigned i = 0; i<HID_KEYBOARD_ROLLOVER; i++) {
            if (keyReport.keys[i] == 0) {
                keyReport.keys[i] = k;
                goto SEND;
            }
        }
        return 0;
    }

SEND:
    keyReport.modifiers |= modifiers;
    sendReport();
    return 1;
}
*/

//https://gist.github.com/MightyPork/6da26e382a7ad91b5496ee55fdc73db2
#include "usb_hid_keys.h"
#define PLED PC13
//#define C2 //PC14 32.768k
//#define C3 //PC15 32.768k
#define C1 PA0
#define C2 PA1
#define C3 PA2
#define C4 PA3
#define C5 PA4
#define C6 PA5
#define C7 PA6
#define C8 PA7
#define CS PB0
#define CR PB1
#define CP PB10
#define CO PB11
#define CN PB12
#define CM PB13
#define CL PB14
#define CK PB15
#define CJ PA8
#define CH PA9
#define CG PA10
// PA11 USB port
// PA12 USB port
#define CF PA15
#define CE PB3
#define CD PB4
#define CC PB5
#define CB PB6
#define CA PB7
#define NUM_KEYS_DEFAULT 78
#define NUM_FN_KEYB_KEYS 16
#define NUM_ALL_KEYS NUM_KEYS_DEFAULT + NUM_FN_KEYB_KEYS
#define delay_ 4

#define MODE_ONLY_KEY 0
#define MODE_CONSUMER 1
#define MODE_KEYBOARD 2
#define MODE_KEYBOARD_D 3
#define MODE_SPECIAL 4

//from HIDReports.cpp
#define REPORT(name, ...)                                              \
  static uint8_t raw_##name[] = {__VA_ARGS__};                         \
  static const HIDReportDescriptor desc_##name = {raw_##name,          \
                                                  sizeof(raw_##name)}; \
  const HIDReportDescriptor *hidReport##name = &desc_##name;

//My own
REPORT(ConsumerKeyboard, HID_KEYBOARD_REPORT_DESCRIPTOR(),
       HID_CONSUMER_REPORT_DESCRIPTOR());

USBHID HID;
HIDKeyboard Keyboard(HID);
HIDConsumer Consumer(HID);

// delay for refresh
int d[NUM_ALL_KEYS];

// state of key in PC
int p[NUM_ALL_KEYS];

// currently grounded pin for key testing
int cur_pin_out = -1;

// Is HID Consumer presses any key
int consumer_action = 0;
int fn_keyb_action = 0;
int fn_keys[NUM_FN_KEYB_KEYS];

bool testfn() {  // 4P
  //digitalWrite(cur_pin_out, HIGH);
  pinMode(cur_pin_out, INPUT_PULLUP);
  pinMode(C4, OUTPUT);
  digitalWrite(C4, LOW);
  bool fn = (digitalRead(CP) == LOW);
  //digitalWrite(C4, HIGH);
  pinMode(C4, INPUT_PULLUP);
  pinMode(cur_pin_out, OUTPUT);
  digitalWrite(cur_pin_out, LOW);
  return fn;
}
void SpecialJob(int sw) {
  switch (sw) {
    case 0:
      Keyboard.print("NVAHVA Keyboard RIP\n11.11.2017-12.1.2019\n\nYuriy Synyshyn Keyboard Reborn\nVer 1.4 19.1.2019\nSTM32F103C8T6 ARM\n32-bit Cortex-M3 CPU 72MHz\n");
      break;
  }
}

void key(int pin, char symbol, int direct, int id, int mode = MODE_ONLY_KEY,
         int keyspecial = 0, int keyspecial_id = 0) {
  if (d[id] > 0)
    d[id]--;
  else {                            // key can be processed after delay
    if (digitalRead(pin) == LOW) {  // key is actually pressed
      d[id] = 4;
      if ((mode == MODE_CONSUMER || mode == MODE_KEYBOARD ||
           mode == MODE_KEYBOARD_D || mode == MODE_SPECIAL) &&
          testfn()) {  // with fn
        if (mode ==
            MODE_CONSUMER) {  // if must process fn_key with HID Consumer
          if (!fn_keys[keyspecial_id]) {  // if not sended yet
            // https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf
            Consumer.press(keyspecial);
            fn_keys[keyspecial_id] = 1;
            consumer_action = 1;
          }
        }  // end fn consumer
        else if (mode == MODE_KEYBOARD ||
                 mode == MODE_KEYBOARD_D) {  // if must process fn_key with HID
                                             // Keyboard
          if (!fn_keys[keyspecial_id]) {     // if not sended yet
            if (mode == MODE_KEYBOARD_D)
              Keyboard.press_direct(keyspecial);
            else
              Keyboard.press(keyspecial);
            fn_keys[keyspecial_id] = 1;
            fn_keyb_action = 1;
          }
        }  // end fn keyboard
        else if (mode == MODE_SPECIAL) {
          SpecialJob(keyspecial);
          d[id] = keyspecial_id;
        }
      }                // end with fn
      else {           // without fn
        if (!p[id]) {  // if wasnt sent before
          if (direct)
            Keyboard.press_direct(symbol);
          else
            Keyboard.press(symbol);
          p[id] = 1;
        }
      }
    }               // end of pressed
    else {          // unpressed
      if (p[id]) {  // need to release key
        if (direct)
          Keyboard.release_direct(symbol);
        else
          Keyboard.release(symbol);
        p[id] = false;  // mark as released
        d[id] = 4;      // delay
      }
      if (mode == MODE_CONSUMER) {  // if must process fn_key with HID Consumer
        if (fn_keys[keyspecial_id]) {
          Consumer.release();
          consumer_action = 0;
          fn_keys[keyspecial_id] = 0;  // mark as released
          d[id] = 4;
        }
      } else if ((mode == MODE_KEYBOARD) ||
                 (mode ==
                  MODE_KEYBOARD_D)) {  // if must process fn_key with HID
                                       // Keyboard
        if (fn_keys[keyspecial_id]) {  // was pressed before
          fn_keys[keyspecial_id] = 0;  // mark as released
          if (mode == MODE_KEYBOARD_D)
            Keyboard.release_direct(keyspecial);
          else
            Keyboard.release(keyspecial);
          fn_keyb_action = 0;
          d[id] = 4;
        }
      }
    }
  }  // end of delay
}  // end of function

void setup() {
  afio_cfg_debug_ports(AFIO_DEBUG_NONE);

  for (int i = 0; i < NUM_ALL_KEYS; i++) {
    d[i] = 0;
    p[i] = 0;
  }
  for (int i = 0; i < NUM_FN_KEYB_KEYS; i++) fn_keys[i] = 0;

  HID.begin(hidReportConsumerKeyboard);
  Keyboard.begin();  // useful to detect host capslock state and LEDs
  pinMode(PC13, OUTPUT);
  /*
  pinMode(C1, OUTPUT);
  pinMode(C2, OUTPUT);
  pinMode(C3, OUTPUT);
  pinMode(C4, OUTPUT);
  pinMode(C5, OUTPUT);
  pinMode(C6, OUTPUT);
  pinMode(C7, OUTPUT);
  pinMode(C8, OUTPUT);
  */
  //digitalWrite(C1, HIGH);
  pinMode(C1, INPUT_PULLUP);
  //digitalWrite(C2, HIGH);
  pinMode(C2, INPUT_PULLUP);
  //digitalWrite(C3, HIGH);
  pinMode(C3, INPUT_PULLUP);
  //digitalWrite(C4, HIGH);
  pinMode(C4, INPUT_PULLUP);
  //digitalWrite(C5, HIGH);
  pinMode(C5, INPUT_PULLUP);
  //digitalWrite(C6, HIGH);
  pinMode(C6, INPUT_PULLUP);
  //digitalWrite(C7, HIGH);
  pinMode(C7, INPUT_PULLUP);
  //digitalWrite(C8, HIGH);
  pinMode(CA, INPUT_PULLUP);

  pinMode(CA, INPUT_PULLUP);
  pinMode(CB, INPUT_PULLUP);
  pinMode(CC, INPUT_PULLUP);
  pinMode(CD, INPUT_PULLUP);
  pinMode(CE, INPUT_PULLUP);
  pinMode(CF, INPUT_PULLUP);
  pinMode(CG, INPUT_PULLUP);
  pinMode(CH, INPUT_PULLUP);
  pinMode(CJ, INPUT_PULLUP);
  pinMode(CK, INPUT_PULLUP);
  pinMode(CL, INPUT_PULLUP);
  pinMode(CM, INPUT_PULLUP);
  pinMode(CN, INPUT_PULLUP);
  pinMode(CO, INPUT_PULLUP);
  pinMode(CP, INPUT_PULLUP);
  pinMode(CR, INPUT_PULLUP);
  pinMode(CS, INPUT_PULLUP);
  
  delay(100);
}

void loop() {
  if (true) {
    digitalWrite(PC13, LOW);

    int i_key = 0, i_fn = 0;  // for handling IDs
    
    pinMode(C1, OUTPUT);
    digitalWrite(C1, LOW);
    cur_pin_out = C1;
    key(CE, KEY_D_F12, 1, i_key++, MODE_KEYBOARD_D, KEY_D_SYSRQ,
        i_fn++);                       // print screen
    key(CG, KEY_D_SLASH, 1, i_key++);  // /
    key(CL, KEY_D_B, 1, i_key++);      // b
    key(CK, KEY_D_N, 1, i_key++);      // n
    key(CP, KEY_RIGHT_ALT, 0, i_key++);
    key(CA, KEY_D_LEFT, 1, i_key++, MODE_KEYBOARD, KEY_HOME, i_fn++);
    key(CS, KEY_D_DOWN, 1, i_key++, MODE_KEYBOARD, KEY_PAGE_DOWN, i_fn++);
    key(CB, KEY_D_RIGHT, 1, i_key++, MODE_KEYBOARD, KEY_END, i_fn++);
    //digitalWrite(C1, HIGH);
    pinMode(C1, INPUT_PULLUP);
    pinMode(C2, OUTPUT);
    digitalWrite(C2, LOW);
    cur_pin_out = C2;
    key(CO, KEY_D_ESC, 1, i_key++);
    key(CM, KEY_D_F4, 1, i_key++, MODE_CONSUMER, HIDConsumer::MUTE, i_fn++);
    key(CJ, KEY_D_F6, 1, i_key++, MODE_CONSUMER, 0xB5, i_fn++);  // next track
    key(CE, KEY_D_F11, 1, i_key++, MODE_KEYBOARD_D, KEY_D_MEDIA_CALC,
        i_fn++);  // calc
    key(CL, KEY_D_G, 1, i_key++);
    key(CK, KEY_D_H, 1, i_key++);
    key(CG, KEY_D_APOSTROPHE, 1, i_key++);  // '
    key(CA, KEY_D_UP, 1, i_key++, MODE_KEYBOARD, KEY_PAGE_UP, i_fn++);
    key(CS, KEY_D_SPACE, 1, i_key++);
    key(CP, KEY_LEFT_ALT, 0, i_key++);
    pinMode(C2, INPUT_PULLUP);
    //digitalWrite(C2, HIGH);
    pinMode(C3, OUTPUT);
    digitalWrite(C3, LOW);
    cur_pin_out = C3;
    key(CE, KEY_D_ENTER, 1, i_key++);
    key(CH, KEY_D_DOT, 1, i_key++);    //.
    key(CJ, KEY_D_COMMA, 1, i_key++);  //,
    key(CK, KEY_D_M, 1, i_key++);
    key(CL, KEY_D_V, 1, i_key++);
    key(CM, KEY_D_C, 1, i_key++);
    key(CN, KEY_D_X, 1, i_key++);
    key(CO, KEY_D_Z, 1, i_key++);
    key(CD, KEY_RIGHT_CTRL, 0, i_key++);
    pinMode(C3, INPUT_PULLUP);
    //digitalWrite(C3, HIGH);
    pinMode(C4, OUTPUT);
    digitalWrite(C4, LOW);
    cur_pin_out = C4;
    key(CE, KEY_D_BACKSLASH, 1, i_key++);  // '\'
    key(CO, KEY_D_A, 1, i_key++);
    key(CN, KEY_D_S, 1, i_key++);
    key(CM, KEY_D_D, 1, i_key++);
    key(CL, KEY_D_F, 1, i_key++);
    key(CK, KEY_D_J, 1, i_key++);
    key(CJ, KEY_D_K, 1, i_key++);
    key(CH, KEY_D_L, 1, i_key++);
    key(CG, KEY_D_SEMICOLON, 1, i_key++);  // ;
    key(CF, KEY_RIGHT_SHIFT, 0, i_key++);
    // key(CP, KEY_LEFT_GUI, i_key++);//fn
    pinMode(C4, INPUT_PULLUP);
    //digitalWrite(C4, HIGH);
    pinMode(C5, OUTPUT);
    digitalWrite(C5, LOW);
    cur_pin_out = C5;
    key(CO, KEY_D_Q, 1, i_key++);
    key(CN, KEY_D_W, 1, i_key++);
    key(CM, KEY_D_E, 1, i_key++);
    key(CL, KEY_D_R, 1, i_key++);
    key(CK, KEY_D_U, 1, i_key++);
    key(CJ, KEY_D_I, 1, i_key++);
    key(CH, KEY_D_O, 1, i_key++);
    key(CG, KEY_D_P, 1, i_key++);
    pinMode(C5, INPUT_PULLUP);
    //digitalWrite(C5, HIGH);
    pinMode(C6, OUTPUT);
    digitalWrite(C6, LOW);
    cur_pin_out = C6;
    key(CM, KEY_D_F3, 1, i_key++, MODE_CONSUMER, 0xE9,
        i_fn++);  // volume increment
    key(CH, KEY_D_F7, 1, i_key++, MODE_CONSUMER, HIDConsumer::PLAY_OR_PAUSE,
        i_fn++);
    key(CE, KEY_D_BACKSPACE, 1, i_key++);
    key(CO, KEY_D_TAB, 1, i_key++);
    key(CL, KEY_D_T, 1, i_key++);
    key(CK, KEY_D_Y, 1, i_key++);
    key(CG, KEY_D_LEFTBRACE, 1, i_key++);   // [
    key(CJ, KEY_D_RIGHTBRACE, 1, i_key++);  // ]
    key(CN, KEY_CAPS_LOCK, 0, i_key++);
    key(CF, KEY_RIGHT_SHIFT, 0, i_key++);
    key(CR, KEY_LEFT_GUI, 0, i_key++);  // windows
    pinMode(C6, INPUT_PULLUP);
    //digitalWrite(C6, HIGH);
    pinMode(C7, OUTPUT);
    digitalWrite(C7, LOW);
    cur_pin_out = C7;
    key(CN, KEY_D_F1, 1, i_key++, MODE_KEYBOARD_D, 0x1C7,
        i_fn++);  // media player
    key(CM, KEY_D_F2, 1, i_key++, MODE_CONSUMER, 0xEA,
        i_fn++);  // volume decrement
    key(CH, KEY_D_F8, 1, i_key++, MODE_CONSUMER, 0xB7, i_fn++);   // stop
    key(CE, KEY_D_F9, 1, i_key++, MODE_CONSUMER, 0x1AA, i_fn++);  // desktop
    key(CS, KEY_D_DELETE, 1, i_key++,MODE_SPECIAL,0,40);
    key(CO, KEY_D_GRAVE, 1, i_key++);  // `
    key(CL, KEY_D_5, 1, i_key++);
    key(CK, KEY_D_6, 1, i_key++);
    key(CG, KEY_D_MINUS, 1, i_key++);    //-
    key(CJ, KEY_D_EQUAL, 1, i_key++);    //+
    key(CD, KEY_LEFT_CTRL, 0, i_key++);  // pb4
    pinMode(C7, INPUT_PULLUP);
    //digitalWrite(C7, HIGH);
    pinMode(C8, OUTPUT);
    digitalWrite(C8, LOW);
    cur_pin_out = C8;
    key(CD, KEY_D_F5, 1, i_key++, MODE_CONSUMER, 0xB6,
        i_fn++);  // pb4 //previous track
    key(CE, KEY_D_F10, 1, i_key++, MODE_KEYBOARD_D, KEY_D_MEDIA_WWW,
        i_fn++);  // control panel
    key(CO, KEY_D_1, 1, i_key++);
    key(CN, KEY_D_2, 1, i_key++);
    key(CM, KEY_D_3, 1, i_key++);
    key(CL, KEY_D_4, 1, i_key++);
    key(CK, KEY_D_7, 1, i_key++);
    key(CJ, KEY_D_8, 1, i_key++);
    key(CH, KEY_D_9, 1, i_key++);
    key(CG, KEY_D_0, 1, i_key++);
    pinMode(C8, INPUT_PULLUP);
    //digitalWrite(C8, HIGH);
    cur_pin_out = -1;
    delay(10);
    digitalWrite(PC13, HIGH);
  }
}
